package com.adamkorzeniak.blockchain.domain;

import lombok.RequiredArgsConstructor;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequiredArgsConstructor
public class MinerMessaging implements Runnable {
    private static final Logger log = Logger.getLogger(MinerMessaging.class.getName());

    private final Miner miner;

    @Override
    public void run() {
        long random = new Random().nextInt(3000);
        if (random < 500) {
            random = 500;
        }
        for (int i = 0; i < 3; i++) {
            try {
                Thread.sleep(random);
            } catch (InterruptedException exc) {
                log.log(Level.SEVERE, exc.getMessage());
                Thread.currentThread().interrupt();
            }
            miner.sendMessage();
        }
    }
}
