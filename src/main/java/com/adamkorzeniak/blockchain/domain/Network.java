package com.adamkorzeniak.blockchain.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Network {
    private final List<Blockchain> blockchains = List.of(new Blockchain(this));
    private final Set<Message> messages = new HashSet<>();

    public Blockchain getLongestBlockchain() {
        return blockchains.get(0);
    }

    public synchronized Set<Message> getMessages() {
        ConcurrentHashMap.KeySetView<Message, Boolean> result = ConcurrentHashMap.newKeySet();
        result.addAll(messages);
        return result;
    }

    public synchronized void addMessage(String message) {
        Message m = new Message(message);
        messages.add(m);
    }

    public synchronized void removeMessages(List<Message> messages) {
        messages.forEach(this.messages::remove);
    }
}
