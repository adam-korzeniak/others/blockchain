package com.adamkorzeniak.blockchain.domain;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Blockchain implements Iterable<Block> {

    private final Network network;
    private int zerosPrefixLength;
    private Block firstBlock;
    private Block lastBlock;

    public Blockchain(Network network) {
        this.network = network;
        this.zerosPrefixLength = 5;
    }

    public synchronized boolean createBlock(String minerId, long magicNumber, List<Message> messages) {
        if (firstBlock == null && !messages.isEmpty()) {
            return false;
        }
        String hash = HashGenerator.generateHash(getLastBlockHash(), magicNumber, messages);
        if (isHashValid(hash)) {
            network.removeMessages(messages);
            Block block = new Block(lastBlock, minerId, magicNumber, zerosPrefixLength, messages);
            Block previousLastBlock = lastBlock;
            lastBlock = block;
            if (previousLastBlock != null) {
                previousLastBlock.setNextBlock(block);
            } else {
                firstBlock = block;
            }
            return true;
        }
        return false;
    }

    public String buildBlockHistory() {
        StringBuilder sb = new StringBuilder();
        for (Block block : this) {
            sb.append(block.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    @Override
    public Iterator<Block> iterator() {
        return new BlockchainIterator(firstBlock);
    }

    public String getLastBlockHash() {
        if (lastBlock == null) {
            return null;
        }
        return lastBlock.getHash();
    }

    public boolean isHashValid(String hash) {
        String prefix = "0".repeat(zerosPrefixLength);
        return hash.startsWith(prefix);
    }

    public boolean isEmpty() {
        return firstBlock == null;
    }
}

class BlockchainIterator implements Iterator<Block> {

    private Block currentBlock;

    public BlockchainIterator(Block firstBlock) {
        this.currentBlock = new Block();
        this.currentBlock.setNextBlock(firstBlock);
    }

    @Override
    public boolean hasNext() {
        return currentBlock != null && currentBlock.getNextBlock() != null;
    }

    @Override
    public Block next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        currentBlock = currentBlock.getNextBlock();
        return currentBlock;
    }
}
