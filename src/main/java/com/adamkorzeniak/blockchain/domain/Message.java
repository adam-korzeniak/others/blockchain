package com.adamkorzeniak.blockchain.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Message {

    @EqualsAndHashCode.Include
    private final String id;
    @Getter
    private final String content;

    public Message(String content) {
        this.id = UUID.randomUUID().toString();
        this.content = content;
    }
}
