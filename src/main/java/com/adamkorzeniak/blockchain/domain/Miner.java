package com.adamkorzeniak.blockchain.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Miner {

    private final Network network;
    private final String id;

    public Miner(Network network) {
        this.network = network;
        this.id = UUID.randomUUID().toString();
    }

    public boolean mineNewBlock() {
        long magicNumber;
        String hash;
        List<Message> messages = new ArrayList<>();
        Blockchain blockchain = network.getLongestBlockchain();
        do {
            String lastBlockHash = blockchain.getLastBlockHash();
            magicNumber = new Random().nextLong();
            messages.clear();
            if (!blockchain.isEmpty()) {
                messages.addAll(network.getMessages());
            }
            hash = HashGenerator.generateHash(lastBlockHash, magicNumber, messages);
        } while (!blockchain.isHashValid(hash));
        return blockchain.createBlock(id, magicNumber, messages);
    }

    public void sendMessage() {
        network.addMessage("Message from miner " + id);
    }
}
