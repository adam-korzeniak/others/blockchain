package com.adamkorzeniak.blockchain.domain;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class Block {

    private static final AtomicLong counter = new AtomicLong();

    private final long id;
    private final long timestamp;
    private final long creationTime;
    private final String minerId;
    private final Block previousBlock;

    @Setter
    @Getter
    private Block nextBlock;
    private String magicNumber;
    @Getter
    private final String hash;
    private int complexity;
    private List<Message> messages;

    public Block() {
        this.id = -1;
        this.timestamp = 0;
        this.creationTime = 0;
        this.previousBlock = null;
        this.minerId = null;
        this.hash = "0";
    }

    public Block(Block previousBlock, String minerId, long magicNumber, int complexity, List<Message> messages) {
        this.id = counter.getAndIncrement();
        this.timestamp = Instant.now().toEpochMilli();
        if (previousBlock == null) {
            this.creationTime = 0;
        } else {
            this.creationTime = timestamp - previousBlock.timestamp;
        }
        this.minerId = minerId;
        this.magicNumber = Long.toString(magicNumber);
        this.previousBlock = previousBlock;
        this.complexity = complexity;
        this.messages = messages;
        this.hash = HashGenerator.generateHash(getPreviousHash(), magicNumber, messages);
    }

    @Override
    public String toString() {
        String previousBlockHash = getPreviousHash();
        return String.format(
                "Block:%n" +
                        "Created by miner %s%n" +
                        "%s gets 100 VC%n" +
                        "Id: %d%n" +
                        "Timestamp: %d%n" +
                        "Magic number:%s%n" +
                        "Hash of the previous block:%n%s%n" +
                        "Hash of the block:%n%s%n" +
                        "Block data:%s%n%s" +
                        "Block was generating for %d seconds%n" +
                        "%s%n",
                minerId, minerId, id, timestamp, magicNumber, previousBlockHash, hash, getNoDataMessage(), getMessages(), creationTime / 1000, getComplexityChangeMessage());
    }

    private String getMessages() {
        if (messages.isEmpty()) {
            return "";
        }
        return messages.stream()
                .map(Message::getContent)
                .collect(Collectors.joining("\n")) + "\n";
    }

    private String getNoDataMessage() {
        return messages.isEmpty() ? " no messages" : "";
    }

    private String getPreviousHash() {
        return previousBlock != null ? previousBlock.hash : "0";
    }

    private String getComplexityChangeMessage() {
        int complexityDiff = getComplexityDiff();

        if (complexityDiff > 0) {
            return String.format("N was increased to %d", complexity);
        } else if (complexityDiff < 0) {
            return String.format("N was decreased by %d", complexity);
        }
        return String.format("N stays the same %d", complexity);
    }

    private int getComplexityDiff() {
        if (previousBlock == null) {
            return complexity;
        }
        return complexity - previousBlock.complexity;

    }
}
