package com.adamkorzeniak.blockchain.domain;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MinerMining implements Runnable {

    private final Miner miner;

    @Override
    public void run() {
        while (!miner.mineNewBlock());
    }
}
