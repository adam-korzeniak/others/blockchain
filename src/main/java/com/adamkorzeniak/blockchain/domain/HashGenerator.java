package com.adamkorzeniak.blockchain.domain;

import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.NoSuchElementException;

@UtilityClass
public class HashGenerator {

    public static String generateHash(String previousHash, long magicNumber, List<Message> messages) {
        List<String> collect = messages.stream()
                .map(Message::getContent)
                .toList();
        MessageDigest digest = getMessageDigest();
        String raw = previousHash + magicNumber + String.join("|", collect);
        byte[] hash = digest.digest(raw.getBytes(StandardCharsets.UTF_8));
        StringBuilder hexString = new StringBuilder();
        for (byte elem : hash) {
            String hex = Integer.toHexString(0xff & elem);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private static MessageDigest getMessageDigest() {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException exc) {
            throw new NoSuchElementException(exc);
        }
        return digest;
    }
}
