package com.adamkorzeniak.blockchain;

import com.adamkorzeniak.blockchain.domain.Miner;
import com.adamkorzeniak.blockchain.domain.MinerMessaging;
import com.adamkorzeniak.blockchain.domain.MinerMining;
import com.adamkorzeniak.blockchain.domain.Network;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class Main {
    private static final Logger log = Logger.getLogger(Main.class.getName());

    private static final int MINERS_COUNT = 15;

    public static void main(String[] args) {
        Network network = new Network();

        ExecutorService executorService = Executors.newCachedThreadPool();
        List<Miner> miners = Stream.generate(() -> new Miner(network))
                .limit(MINERS_COUNT)
                .toList();

        miners.stream()
                .map(MinerMining::new)
                .forEach(executorService::execute);

        miners.stream()
                .map(MinerMessaging::new)
                .forEach(executorService::execute);

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException exc) {
            log.log(Level.INFO, exc.getMessage());
            Thread.currentThread().interrupt();
        }
        log.log(Level.INFO, () -> "\n" + network.getLongestBlockchain().buildBlockHistory());
    }
}
